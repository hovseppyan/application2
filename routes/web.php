<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('users', 'UsersController@users');
Route::post('users/delete/{id}', 'UsersController@destroy')->name('destroy');
Route::get('users/add', function (){
    return view('adduser');
});
Route::post('users/add', 'UsersController@add');

Route::get('users/update', function (){
    return view('updateuser');
});

Route::post('/users/update/{id}', 'UsersController@update');

