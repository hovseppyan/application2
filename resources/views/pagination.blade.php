<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html lang="en" xmlns:http="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <title>Document</title>
</head>
<body>
<div class="container">
<table class="table table-dark">
    <thead>
    <tr>
        <th scope="col">Id</th>
        <th scope="col">Name</th>
        <th scope="col">Email</th>
        <th scope="col">Delete  User</th>



    </tr>
    </thead>
    <tbody>
    @foreach($users as $user)
    <tr>
        <td>{{$user->id}}</td>
        <td>{{$user->name}}</td>
        <td>{{$user->email}}</td>
        <th>

            <form action="{{route('destroy', $user->id)}}" method="post">
                @csrf
            <button type="submit">DELETE</button>
            </form>
            <form action="users/update" method="get">
                @csrf
                <button type="submit">UPDATE</button>
            </form>

        </th>



    </tr>

    @endforeach



    </tbody>
</table>
    {{$users->links()}}
    <a href="http://127.0.0.1:8000/" style="font-size: 28px">Home</a>
    <a href="http://127.0.0.1:8000/users/add" style="font-size: 28px"> Add</a>

</div>
</body>
</html>
