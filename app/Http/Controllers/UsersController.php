<?php

namespace App\Http\Controllers;

use App\Http\Requests\AddRequest;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class UsersController extends Controller
{
    public function add(AddRequest $addRequest){
        return User::create([
            'name' => $addRequest['name'],
            'email' => $addRequest['email'],
            'password' => Hash::make($addRequest['password']),
        ]);





    }
    public function update($id, AddRequest $request)
    {
        $user = User::findOrFail($id);

        $user->name = $request->get('name');

        $user->email = $request->get('email');

        $user->save();

        return \Redirect::route('users.update', [$user->id])->with('message', 'User has been updated!');
    }


    public function destroy(Request $request, $id)
    {
        $user = User::findOrFail($id);
        $user->delete();
        return redirect('/users')->with('success', 'User has been deleted');
    }

    public function users(){
        $users = User::paginate(3);
        return view('pagination', compact('users'));
    }
}
